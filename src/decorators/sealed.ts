export function sealed( param ) {
    return function( target: Function ) {
        console.log( 'Sealing the constructor ', param );
        Object.seal(target);
        Object.seal(target.prototype);
    };
}
