export function logParameter( target: any, method: string, index: number ) {
    const key = `${ method }_decor_params_indexes`;

    if( Array.isArray( target[key] ) ) {
        target[key].push( index );
    } else {
        target[key] = [ index ];
    }
}
