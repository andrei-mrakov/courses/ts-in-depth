export function logger<F extends Function>( target: F ): F {
    const newConstructor = function() {
        console.log( 'Creating new instance' );
        console.log( 'Class name: ', target.name );
        this.age = 30;
    };

    newConstructor.prototype = Object.create( target.prototype );
    newConstructor.prototype.printLibrarian = function() {
        console.log( `Librarian name:  ${ this.name }, Librarian age: ${ this.age }` );
    };

    return newConstructor as F;
}
