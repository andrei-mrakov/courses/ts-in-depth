export function timeout( ms = 0 ) {
    return function( target: any, method: string, descriptor: PropertyDescriptor ) {
        const originalMethod = descriptor.value;

        descriptor.value = function( ...args: any ) {
            setTimeout( () => {
                originalMethod.apply( this, args );
            }, ms );
        };

        return descriptor;
    };
}
