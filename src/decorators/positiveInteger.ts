export function positiveInteger( target: any, method: string, descriptor: PropertyDescriptor ) {
    const originalSetter = descriptor.set;

    descriptor.set = function( value: number ) {
        if( value < 1 || !Number.isInteger( value ) ) {
            throw new Error( 'Invalid value.' );
        }

        originalSetter.call(this, value);
    };

    return descriptor;
}
