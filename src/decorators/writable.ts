export function writable(isWritable: boolean) {
    return function ( target: any, method: string, descriptor: PropertyDescriptor ) {
        console.log( 'target', target );
        console.log( 'method', method );
        console.log( 'descriptor', descriptor );

        descriptor.writable = isWritable;

        return descriptor;
    };
}
