export function logMethod( target: any, method: string, descriptor: PropertyDescriptor ) {
    const originalMethod = descriptor.value;
    const key = `${ method }_decor_params_indexes`;

    descriptor.value = function( ...args: any ) {
        const indexes = target[key];

        if( Array.isArray( indexes ) ) {
            args.forEach( ( arg, index ) => {
                if( indexes.includes( index ) ) {
                    console.log( `Method: ${ method }, ParamIndex: ${ index }, ParamValue: ${ arg }` );
                }
            } );
        }

        return originalMethod.apply(this, args);
    };

    return descriptor;
}
