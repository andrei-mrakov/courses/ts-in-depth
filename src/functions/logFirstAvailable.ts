import { Book } from '@/types';
import { getAllBooks } from './getAllBooks';

export function logFirstAvailable( books: readonly Book[] = getAllBooks() ): void {
    const firstAvailable = books.find( book => book.available );

    console.log( 'books length = ', books.length );
    if( firstAvailable ) {
        console.log( 'First available book title: ', firstAvailable.title );
    } else {
        console.log( 'no available books...' );
    }
}
