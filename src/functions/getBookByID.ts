import { Book } from '@/interfaces';
import { BookOrUndefined } from '@/types';
import { getAllBooks } from './getAllBooks';

export function getBookByID( id: Book['id'] ): BookOrUndefined {
    return getAllBooks().find( book => book.id === id );
}
