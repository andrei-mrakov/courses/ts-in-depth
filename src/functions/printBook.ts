import { Book } from '@/interfaces';
import { getAllBooks } from './getAllBooks';

export function printBook( book: Book ): void {
    console.log( `${ book.title } by ${ book.author }` );
}
