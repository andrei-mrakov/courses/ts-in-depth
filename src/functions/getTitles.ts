/* eslint-disable no-redeclare */
import { Book } from '@/types';
import { getAllBooks } from './getAllBooks';

export function getTitles( author: string ): Book[];
export function getTitles( available: boolean ): Book[];
export function getTitles( id: Book['id'], available: Book['available'] ): Book[];

export function getTitles( ...args: any[] ) {
    return getAllBooks().reduce((acc,book) => {
        if( typeof args[0] === 'string' && book.author === args[0] ) {
            return [...acc, book.title];
        }

        if( typeof args[0] === 'boolean' && book.available === args[0]) {
            return [...acc, book.title];
        }

        if( typeof args[0] === 'number' &&  typeof args[1] === 'boolean' && book.id === args[0] && book.available === args[1]) {
            return [...acc, book.title];
        }

        return acc;
    },[]);

}
