export function logBookTitles<T = any>( books: readonly T[] = [] ): void {
    if( !Array.isArray( books ) || !books.length ) {
        console.log( 'Invalid "boots" argument' );
    }
    console.group( 'Books: ' );
    books.forEach( ( book, index ) => {
        // @ts-ignore
        console.log( `${ index + 1 }. ${ book.title }` );
    } );
    console.groupEnd();
}
