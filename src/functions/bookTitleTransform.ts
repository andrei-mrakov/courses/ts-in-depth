import { assertStringValue } from './assertStringValue';

export function bookTitleTransform( title: any ) {
    return [...assertStringValue(title)].reverse().join('');
}
