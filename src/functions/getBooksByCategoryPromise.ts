import { Category } from '@/enums';
import { Book } from '@/interfaces';
import { getBookTitlesByCategory } from './getBookTitlesByCategory';

export function getBooksByCategoryPromise(category: Category): Promise<Book['title'][]> {
    return new Promise<Book['title'][]>((resolve,reject)=>{
        setTimeout( () => {
            const titles = getBookTitlesByCategory( category );

            return titles.length ? resolve(titles) : reject('No books found.');
        }, 2000 );
    });
}
