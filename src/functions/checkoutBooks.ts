import { Book } from '@/types';
import { getBookByID } from './getBookByID';

export function checkoutBooks( customer: string, ...bookIDs: Book['id'][] ) {
    console.log( 'client: ', customer );
    return bookIDs.map( getBookByID ).reduce( ( acc, book ) => {
        if( book?.available ) {
            acc.push( book.title );
        }

        return acc;
    }, [] );
}
