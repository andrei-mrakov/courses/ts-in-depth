import { LibMgrCallback } from '@/interfaces';
import { Category } from '@/enums';
import { getBookTitlesByCategory } from './getBookTitlesByCategory';

export function getBooksByCategory( category: Category, cb: LibMgrCallback ) {
    setTimeout( () => {
        try {
            const titles = getBookTitlesByCategory( category );
            if(!titles.length){
                throw new Error('No books found.'); return cb(null, titles);
            }

            cb(null, titles);

        } catch ( e ) {
            cb(e, null);
        }
    }, 2000 );
}
