export function createCustomer(name: string, age?: number, city?: string){
    const str1 = `name: ${name}`;
    const str2 = `${str1}, age: ${age}`;
    const str3 = `${str2}, city: ${city}`;

    switch ( arguments.length ){
        case 1:
            console.log( str1 );
            break;
        case 2:
            console.log( str2 );
            break;
        case 3:
        default:
            console.log( str3 );
            break;
    }
}
