import { Book } from '../types';
import { getAllBooks } from './getAllBooks';

export function getBookAuthorByIndex(index: number): [Book['title'], Book['author']]{
    const books = getAllBooks();
    const foundedBook = books[index];
    if(!foundedBook){
        throw Error(`Book with index "${index}" was not found`);
    }
    console.log( `Book with index "${index}": `,[foundedBook.title, foundedBook.author] );
    return [foundedBook.title, foundedBook.author];
}
