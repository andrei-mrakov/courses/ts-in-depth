export function createCustomerID(name: string, age: number) {
    return `${name}-${age}`;
}
