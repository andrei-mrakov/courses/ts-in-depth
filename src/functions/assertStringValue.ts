export function assertStringValue(str: any): string | never {
    if(typeof str !== 'string'){
        throw new TypeError(`value should be a string, but you pass ${typeof str}`);
    }

    return str;
}
