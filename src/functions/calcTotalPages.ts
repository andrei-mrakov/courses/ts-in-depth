const library = [
    { lib: 'libName1', books: 1_000_000_000, avgPagesPerBook: 250 },
    { lib: 'libName2', books: 5_000_000_000, avgPagesPerBook: 300 },
    { lib: 'libName3', books: 3_000_000_000, avgPagesPerBook: 280 },
] as const;

export function calcTotalPages(): bigint {
    const totalPages = library.reduce((prev,curr)=>{
        return prev + BigInt(curr.avgPagesPerBook) * BigInt(curr.books);
    }, 0n);

    console.log( 'Total pages: ', totalPages );

    return totalPages;
}
