import { Category } from '../enums';
import { getAllBooks } from './getAllBooks';

export function getBookTitlesByCategory(category= Category.Javascript): string[] {
    return getAllBooks()
        .filter(book => book.category === category)
        .map(book => book.title);
}
