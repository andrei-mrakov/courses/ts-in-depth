import { Category } from '@/enums';
import { getBooksByCategoryPromise } from './getBooksByCategoryPromise';

export async function logSearchResults (category: Category){
    const titles = await getBooksByCategoryPromise(category);
    console.log( 'number of books:', titles.length );
}
