import { Category } from '@/enums';
import { Book } from '@/interfaces';

const books = [
    {
        id: 1,
        title: 'Refactoring JavaScript',
        author: 'Evan Burchard',
        available: true,
        category: Category.HTML
    },
    {
        id: 2,
        title: 'JavaScript Testing',
        author: 'Liang Yuxian Eugene',
        available: false,
        category: Category.Javascript
    },
    {
        id: 3,
        title: 'CSS Secrets',
        author: 'Lea Verou',
        available: true,
        category: Category.CSS
    },
    {
        id: 4,
        title: 'Mastering JavaScript Object-Oriented Programming',
        author: 'Andrea Chiarelli',
        available: true,
        category: Category.Typescript
    },
] as const;

export function getAllBooks(): readonly any[] {
    return books;
}
