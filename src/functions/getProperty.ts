export function getProperty<O, K extends keyof O>(book: O, property: K): O[K] | string {
    const value = book[property];

    // @ts-ignore
    return typeof value !== 'function' ? value : property;
}
