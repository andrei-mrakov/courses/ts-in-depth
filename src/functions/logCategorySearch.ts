import { LibMgrCallback } from '@/interfaces';

export const logCategorySearch: LibMgrCallback = ( err, titles ) => {
    if( err ) {
        return console.log( 'err.message: ', err.message );
    }

    console.log( 'titles:', titles );
};
