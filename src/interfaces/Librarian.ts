import { Person } from './Person';

export interface Librarian extends Person {
    department: string;
    assistCustomer: ( name: string ) => void;
}
