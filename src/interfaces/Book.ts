import { Category } from '@/enums';
import { DamageLogger } from './DamageLogger';

export interface Book {
    id: number;
    title: string;
    author: string;
    available: boolean;
    category: Category;
    pages?: number;
    markDamaged: DamageLogger;
}
