export { Magazine } from './Magazine';
export { Book } from './Book';
export { DamageLogger } from './DamageLogger';
export { Person } from './Person';
export { Author } from './Author';
export { Librarian } from './Librarian';
export { ShelfItem } from './ShelfItem';
export { LibMgrCallback } from './LibMgrCallback';
