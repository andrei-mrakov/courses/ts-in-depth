export interface LibMgrCallback {
    (error: Error, titles: string[]): void;
}
