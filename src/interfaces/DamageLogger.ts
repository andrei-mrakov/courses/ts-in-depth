export interface DamageLogger {
    (str: string): void;
}
