import { Person } from './Person';

export interface Author extends Person{
    numBooksPublished: number;
}
