export interface Magazine {
    title: string;
    publisher: string;
}
