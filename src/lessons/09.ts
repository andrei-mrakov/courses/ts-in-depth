import { Category } from '@/enums';
import {
    getBooksByCategory,
    getBooksByCategoryPromise,
    logCategorySearch,
    logSearchResults
} from '@/functions';

export function p09() {
    console.group( 'part 09' );
    console.group( '09.01' );

    // 5
    console.log( 'Before calling getBooksByCategory' );
    getBooksByCategory(Category.Javascript, logCategorySearch);
    console.log( 'After calling getBooksByCategory' );
    console.log( 'Before calling getBooksByCategory' );
    getBooksByCategory(Category.Software, logCategorySearch);
    console.log( 'After calling getBooksByCategory' );
    console.groupEnd();
    // ---------------------------
    console.group( '09.02' );
    // 3-4
    console.log( 'Before calling getBooksByCategoryPromise' );
    getBooksByCategoryPromise(Category.Javascript)
        .then(titles=> titles.length)
        .then(console.log)
        .catch(console.error);
    console.log( 'After calling getBooksByCategoryPromise' );
    console.log( 'Before calling getBooksByCategoryPromise' );
    getBooksByCategoryPromise(Category.Software)
        .then(titles=> titles.length)
        .then(console.log)
        .catch(console.error);
    console.log( 'After calling getBooksByCategoryPromise' );
    console.groupEnd();
    // ---------------------------
    console.group( '09.03' );
    // 2
    console.log( 'Before calling logSearchResults' );
    logSearchResults(Category.Javascript).catch(console.error);
    console.log( 'After calling logSearchResults' );
    console.groupEnd();
    console.groupEnd();
}
