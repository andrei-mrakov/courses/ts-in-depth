import {
    checkoutBooks,
    createCustomer,
    createCustomerID,
    getBookByID,
    getBookTitlesByCategory,
    logFirstAvailable,
    getTitles,
    bookTitleTransform,
} from '@/functions';

export function p03() {
    console.group( 'part 03' );
    console.group( '03.01' );
    // 2
    console.log( 'myID: ', createCustomerID( 'Ann', 10 ) );
    // 3 - 4
    const idGenerator: ( name: string, age: number ) => string = createCustomerID;
    console.log( 'idGenerator: ', idGenerator( 'Boris', 20 ) );

    console.groupEnd();
    // ---------------------------
    console.group( '03.02' );

    // 1
    createCustomer( 'Vladilen' );
    createCustomer( 'Vladilen', 27 );
    createCustomer( 'Vladilen', 27, 'Saint Petersburg' );

    // 2
    console.log( 'modified "getBookTitlesByCategory" with default param: ', getBookTitlesByCategory() );

    // 3
    logFirstAvailable();

    // 4
    getBookByID( 1 );

    // 6
    console.log( 'available books:', checkoutBooks( 'Ann', 1, 2, 4 ) );

    console.groupEnd();
    // ---------------------------
    console.group( '03.03' );

    // 3
    console.log( 'unavailable books: ', getTitles( false ) );

    console.groupEnd();
    // ---------------------------
    console.group( '03.04' );

    // 3
    console.log( 'reversed title: ', bookTitleTransform('Harry Potter') );
    // console.log( 'reversed title: ', bookTitleTransform(123) );

    console.groupEnd();
    console.groupEnd();
}
