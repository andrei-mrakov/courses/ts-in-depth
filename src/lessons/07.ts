import { Shelf } from '@/classes';
import { Category } from '@/enums';
import { createCustomer, getProperty, purge } from '@/functions';
import { Magazine } from '@/interfaces';
import { Book, BookRequiredFields, CreateCustomerFunctionType, UpdatedBook } from '@/types';

export function p07() {
    console.group( 'part 07' );
    console.group( '07.01' );

    // 4
    const inventory: Book[] = [
        { id: 10, title: 'The C Programming Language', author: 'K & R', available: true, category: Category.Software },
        { id: 11, title: 'Code Complete', author: 'Steve McConnell', available: true, category: Category.Software },
        { id: 12, title: '8-Bit Graphics with Cobol', author: 'A. B.', available: true, category: Category.Software },
        { id: 13, title: 'Cool autoexec.bat Scripts!', author: 'C. D.', available: true, category: Category.Software },
    ];

    // 5
    console.log( 'inventory: ', purge( inventory ) );

    // 6
    console.log( 'inventory2: ', purge( [ 1, 2, 3, 4, 5 ] ) );

    console.groupEnd();
    // ---------------------------
    console.group( '07.02' );

    // 6
    const bookShelf = new Shelf<Book>();
    inventory.forEach( bookShelf.add );
    console.log( 'first boot from shell: ', bookShelf.first );

    // 7
    const magazines: Magazine[] = [
        { title: 'Programming Language Monthly', publisher: 'Code Mags' },
        { title: 'Literary Fiction Quarterly', publisher: 'College Press' },
        { title: 'Five Points', publisher: 'GSU' },
    ];

    // 8
    const magazineShelf = new Shelf<Magazine>();
    magazines.forEach( magazineShelf.add );
    console.log( 'first magazine from shell: ', magazineShelf.first );

    console.groupEnd();
    // ---------------------------
    console.group( '07.03' );

    // 4
    magazineShelf.printTitles();
    console.log( 'Five Points: ', magazineShelf.find( 'Five Points' ) );

    // 6
    console.log( getProperty( magazines[0], 'title' ) );

    console.groupEnd();
    // ---------------------------
    console.group( '07.04' );

    // 2
    const bookRequiredFields: BookRequiredFields = {
        id: 777,
        title: 'Три поросёнка',
        category: Category.Software,
        available: true,
        author: 'Сергей Михалков',
    };

    // 4
    const updatedBook: UpdatedBook = {
        author: 'Unknown',
    };

    // 7
    const createCustomerParams: Parameters<CreateCustomerFunctionType> = [ 'Volha', 27, 'Minsk' ];
    createCustomer( ...createCustomerParams );

    console.groupEnd();
    console.groupEnd();
}
