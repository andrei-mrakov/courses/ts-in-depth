import {
    getProperty,
    printBook,
} from '@/functions';
import { Category } from '@/enums';
import { Author, Book, Librarian } from '@/interfaces';

export function p04() {
    console.group( 'part 04' );
    console.group( '04.01' );

    // 5
    const myBook: Book = {
        id: 5,
        title: 'Colors, Backgrounds, and Gradients',
        author: 'Eric A. Meyer',
        available: true,
        category: Category.CSS,
        // year: 2015,
        // copies: 3
        pages: 200,
        markDamaged: reason => {
            console.log( 'Damaged: ', reason );
        }
    };

    printBook(myBook);

    // 9
    myBook.markDamaged('missing back cover');

    console.groupEnd();
    // ---------------------------
    console.group( '04.02' );

    // 3
    const logDamage: Book['markDamaged'] = (reason)=> {
        console.log( 'reason', reason );
    };

    logDamage('no reason');

    console.groupEnd();
    // ---------------------------
    console.group( '04.03' );

    // 4
    const favoriteAuthor: Author = {
        name: 'Tom Cruise',
        email: 'tom.cruise@mission.impossible',
        numBooksPublished: 7
    };
    console.log( 'author: ', favoriteAuthor );

    // 5
    const favoriteLibrarian: Librarian = {
        name: 'Brus Wayne',
        email: 'brus.wayne@batman.net',
        department: 'some department',
        assistCustomer: name => {
            console.log( 'name', name );
        }
    };

    console.log( 'librarian: ', favoriteLibrarian );

    console.groupEnd();
    // ---------------------------
    console.group( '04.04' );

    // 1
    const offer: any = {
        book: {
            title: 'Essential TypeScript',
        },
    };
    // 2
    console.log( 'offer.magazine :', offer.magazine );
    console.log( 'offer.magazine.getTitle() :', offer.magazine?.getTitle?.() );
    console.log( 'offer.book.getTitle() :', offer.book?.getTitle?.() );
    console.log( 'offer.book.authors[0] :', offer.book?.authors?.[0] );

    console.groupEnd();
    // ---------------------------
    console.group( '04.05' );

    // 3
    console.log( 'my book\'s title: ', getProperty(myBook, 'title') );
    console.log( 'my book\'s markDamaged: ', getProperty(myBook, 'markDamaged') );
    // @ts-ignore
    console.log( 'my book\'s isbn: ', getProperty(myBook, 'isbn') );
    console.groupEnd();
    console.groupEnd();
}
