import { Encyclopedia, UniversityLibrarian } from '@/classes';

export function p08() {
    console.group( 'part 08' );
    console.group( '08.01' );

    // 3
    const librarian = new UniversityLibrarian();
    console.log( 'librarian: ', librarian );
    console.groupEnd();
    // ---------------------------
    console.group( '08.02' );
    // 7
    const fLibrarian = new UniversityLibrarian();
    fLibrarian.name = 'Anna';
    fLibrarian['printLibrarian']?.();
    console.groupEnd();
    // ---------------------------
    console.group( '08.03' );
    // 4
    const anotherLibrarian = new UniversityLibrarian();
    anotherLibrarian.assistFaculty = null;
    // anotherLibrarian.teachCommunity = null;
    console.log( 'anotherLibrarian:', anotherLibrarian );
    console.groupEnd();
    // ---------------------------
    console.group( '08.04' );
    // 4
    const encyclopedia = new Encyclopedia({
        title: 'Some book',
        id: 322,
        year: 2021,
        edition: '1.2'
    });
    encyclopedia.printItem();
    console.groupEnd();
    // ---------------------------
    console.group( '08.05' );
    // 5
    const ulib = new UniversityLibrarian('08.05');
    ulib.assistCustomer('Boris');
    console.log( 'ulib: ', ulib );
    console.groupEnd();
    // ---------------------------
    console.group( '08.06' );
    // 5
    const ulib2 = new UniversityLibrarian('08.06');
    ulib2.assistCustomer('Boris');
    console.log( 'ulib2: ', ulib2 );
    console.groupEnd();
    // ---------------------------
    console.group( '08.07' );
    // 4
    const encyclopedia1 = new Encyclopedia({});
    // encyclopedia1.copies = -10;
    // console.log( 'encyclopedia1.copies: ', encyclopedia1.copies );
    // encyclopedia1.copies = 0;
    // console.log( 'encyclopedia1.copies: ', encyclopedia1.copies );
    // encyclopedia1.copies = 4.5;
    // console.log( 'encyclopedia1.copies: ', encyclopedia1.copies );
    encyclopedia1.copies = 5;
    console.log( 'encyclopedia1.copies: ', encyclopedia1.copies );
    console.groupEnd();
    console.groupEnd();
}
