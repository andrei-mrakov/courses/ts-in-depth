import { Library, UniversityLibrarian } from '@/classes';

export function p06(){
    console.group( 'part 06' );
    console.group( '06.05' );

    import('@/classes/Reader').then( ({ Reader })=>{
        const reader = new Reader('Mario');
        console.log( 'reader: ', reader );
    });

    console.groupEnd();
    // ---------------------------
    console.group( '06.06' );

    // 3
    // const library: Library = new Library(2048, 'National Library','near East station');

    const library: Library = {
        id: 2048,
        name: 'National Library',
        address: 'near East station'
    };
    console.log( 'library: ', library );

    console.groupEnd();
    console.groupEnd();
}
