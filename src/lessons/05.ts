import { Encyclopedia, UniversityLibrarian } from '@/classes';
import { Librarian } from '@/interfaces';
import { Category } from '@/enums';
import { PersonBook } from '@/types';

export function p05() {
    console.group( 'part 05' );
    console.group( '05.01' );

    // 2
    // const ref = new ReferenceItem('Artemis Faul', 2008, 322);
    // ref.printItem();

    // 4с
    // ref.publisher = 'Оуэн Колфер';
    // console.log( 'ref publisher: ', ref.publisher );

    // 5c
    // console.log( 'ref: ', ref );

    // 5d
    // console.log( 'ref id: ', ref.getID() );
    console.groupEnd();
    // ---------------------------
    console.group( '05.02' );

    // 2
    const refBook = new Encyclopedia( {
        title: '008 Agent',
        year: 1998,
        id: 400,
        edition: 'basic',
    } );

    refBook.printItem();

    console.groupEnd();
    // ---------------------------
    console.group( '05.03' );

    // 4
    refBook.printCitation();
    console.groupEnd();
    // ---------------------------
    console.group( '05.04' );

    // 2
    const favoriteLibrarian: Librarian = new UniversityLibrarian(
        'Brad Pitt',
        'brad.pitt@gmail.com',
        'Hollywood',
    );

    favoriteLibrarian.assistCustomer( 'Angelina Jolie' );

    console.groupEnd();
    // ---------------------------
    console.group( '05.05' );

    // 2
    const personBook: PersonBook = {
        name: 'Питер',
        title: 'Грокаем Алгоритмы',
        email: 'some.email@mail.net',
        pages: 288,
        author: 'Адитья Бхаргава',
        available: true,
        category: Category.Software,
        id: 2021,
        markDamaged: str => {
            console.log( 'Damaged with ', str );
        }
    };
    console.log( 'person book: ', personBook );

    console.groupEnd();
    console.groupEnd();
}
