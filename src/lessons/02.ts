import {
    calcTotalPages,
    getAllBooks,
    getBookAuthorByIndex,
    getBookTitlesByCategory,
    logBookTitles,
    logFirstAvailable,
} from '@/functions';
import { Category } from '@/enums';
import { Book } from '@/types';

export function p02() {
    console.group( 'part 02' );
    console.group( '02.01' );

    // 2
    logFirstAvailable( getAllBooks() );
    // 6
    getBookTitlesByCategory( Category.Typescript );
    // 7
    logBookTitles<Book>( getAllBooks() );
    // 8
    getBookAuthorByIndex( ~~( Math.random() * getAllBooks().length ) );
    // 9
    calcTotalPages();

    console.groupEnd();
    console.groupEnd();
}
