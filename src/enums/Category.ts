export enum Category {
    Javascript,
    CSS,
    HTML,
    Typescript,
    Angular,
    Software
}
