import { Book } from './Book';

export type UpdatedBook = Partial<Book>;
