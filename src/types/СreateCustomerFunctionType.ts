import { createCustomer } from '@/functions';

export type CreateCustomerFunctionType = typeof createCustomer;
