import { Book } from '@/interfaces';

export type BookProperties = keyof Book;
