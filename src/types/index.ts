export { Book } from './Book';
export { BookProperties } from './BookProperties';
export { PersonBook } from './PersonBook';
export { BookOrUndefined } from './BookOrUndefined';
export { BookRequiredFields  } from './BookRequiredFields';
export { UpdatedBook  } from './UpdatedBook';
export { AuthorWoEmail  } from './AuthorWoEmail';
export { CreateCustomerFunctionType  } from './СreateCustomerFunctionType';
