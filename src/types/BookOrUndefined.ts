import { Book } from './Book';

export type BookOrUndefined = Book | undefined;
