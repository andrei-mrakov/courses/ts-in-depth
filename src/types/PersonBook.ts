import { Person, Book } from '@/interfaces';

export type PersonBook = Person & Book;
