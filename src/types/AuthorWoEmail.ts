import { Author } from '@/interfaces';

export type AuthorWoEmail = Omit<Author, 'name' | 'email' | 'numBooksPublished'>;
