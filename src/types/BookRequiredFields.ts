import { Book } from './Book';

export type BookRequiredFields = Required<Book>;
