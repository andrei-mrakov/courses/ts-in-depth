import { timeout } from '@/decorators';

export abstract class ReferenceItem {
    // title: string;
    // year: number;
    _publisher: string;
    static department = 'School';

    protected constructor( public title: string, protected year: number, private id: number ) {
        // this.title = title;
        // this.year = year;
        console.log( 'Creating a new ReferenceItem...' );
    }

    @timeout(2000)
    printItem(): void {
        console.log( `${ this.title } was published in ${ this.year } at ${ ReferenceItem.department }` );
    }

    get publisher() {
        // eslint-disable-next-line no-underscore-dangle
        return this._publisher.toUpperCase();
    }

    set publisher( newPublisher ) {
        // eslint-disable-next-line no-underscore-dangle
        this._publisher = newPublisher;
    }

    getID() {
        return this.id;
    }

    abstract printCitation(): void;
}
