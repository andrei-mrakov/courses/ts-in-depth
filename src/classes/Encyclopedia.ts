import { positiveInteger } from '@/decorators';
import { ReferenceItem } from './ReferenceItem';

export class Encyclopedia extends ReferenceItem {
    edition: string;
    private _copies: number;

    constructor( { title, year, id, edition }: { title: string; year: number; id: number; edition: string } ) {
        super( title, year, id );
        this.edition = edition;
        // NOTE: no error on private year field
    }

    printItem() {
        super.printItem();
        console.log( `Edition: edition ${ this.edition }` );
    }

    printCitation() {
        console.log( `${ this.title } - ${ this.year }` );
    }

    get copies() {
        return this._copies;
    }

    @positiveInteger
    set copies( value ) {
        this._copies = value;
    }
}
