import { ShelfItem } from '@/interfaces';

export class Shelf<T extends ShelfItem> {
    private items: T[] = [];

    add = ( item: T ) => {
        this.items.push( item );
    };

    get first(): T {
        return this.items[0];
    }

    find( title: string ): T {
        return this.items.find( item => item.title === title );
    }

    printTitles() {
        this.items.forEach( ( item, index ) =>
            console.log( `Title with index = ${ index } is ${ item.title }` ) );
    }
}
