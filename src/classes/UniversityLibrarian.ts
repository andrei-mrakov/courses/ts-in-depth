import { Librarian } from '@/interfaces';
import { format, logger, logMethod, logParameter, sealed, writable } from '@/decorators';

@logger
// @sealed('UniversityLibrarian')
export class UniversityLibrarian implements Librarian {
    @format() name: string;

    constructor( name: string, public email: string, public department: string ) {
        this.name = name;
    }

    @logMethod
    assistCustomer( @logParameter name: string ) {
        console.log( `${ this.name } is assisting ${ name }` );
    }

    // @writable( true )
    assistFaculty() {
        console.log( 'Assisting faculty' );
    }

    // @writable( false )
    teachCommunity() {
        console.log( 'Teaching community' );
    }
}
